import React from "react";
import "./App.scss";
import Currency from "./components/Currency";

function App() {
  return (
    <section>
      <Currency />
    </section>
  );
}

export default App;
