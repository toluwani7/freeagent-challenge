import React, { useEffect, useState } from "react";
import styles from "./style.module.scss";
import { Button } from "../../../../lib";
import config from "../../../../config";
import moment from "moment";
import { getHistoricalData } from "../../../../services";

const sampleData = {
  success: true,
  timestamp: 1387929599,
  historical: true,
  base: "EUR",
  date: "2013-12-24",
  rates: { USD: 1.367761, CAD: 1.453867 },
};

const Result = ({ amount, selectedCurrencies, resetForm }) => {
  const [currency1, currency2] = selectedCurrencies;
  const [historicData, sethistoricData] = useState([]);

  const getData = async (daysSubtracted) => {
    return await getHistoricalData(
      selectedCurrencies,
      moment().subtract(daysSubtracted, "days").format("YYYY-MM-DD")
    );
  };

  useEffect(() => {
    const fetchRequest = () => {
      const hd1 = getData(1);
      const hd2 = getData(2);
      const hd3 = getData(3);
      const hd4 = getData(4);
      const hd5 = getData(5);
      Promise.all([hd1, hd2, hd3, hd4, hd5]).then((values) => {
        const data = values.map((item) => {
          const { date, rates } = item;
          const {
            [selectedCurrencies[0]]: id1,
            [selectedCurrencies[1]]: id2,
          } = rates;

          return {
            date,
            id1: id1,
            id2: id2,
          };
        });
        sethistoricData(data);
      });
    };
    fetchRequest();
  }, []);

  // const { rates } = historicData;
  // const ratekeys = Object.keys(rates);

  // const ratesOutput = ratekeys.map((key) => {
  //   return rates[key];
  // });

  return (
    <div className={styles.wrapper}>
      <div className={styles.currencyInputResult}>
        <h3 className={styles.resultText}>{amount} EUR</h3>
        <p>(amount entered in step 1)</p>
      </div>
      <div className={styles.resultContainer}>
        <table className={styles.resultTable}>
          <tr>
            <th>Date</th>
            <th>{currency1}</th>
            <th>{currency2}</th>
          </tr>
          {historicData.map(({ date, id1, id2 }) => (
            <tr>
              <td>{date.replace("/", "-")}</td>
              <td>{(id1 * amount).toFixed(2)}</td>
              <td>{(id2 * amount).toFixed(2)}</td>
            </tr>
          ))}
        </table>
      </div>
      <div>
        <div className={styles.buttonContainer}>
          <Button text={"Reset form"} clickHandler={resetForm} />
        </div>
      </div>
    </div>
  );
};

export default Result;
