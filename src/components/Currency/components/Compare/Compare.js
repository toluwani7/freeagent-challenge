import React, { useEffect, useState } from "react";
import styles from "./style.module.scss";
import { Button } from "../../../../lib";
import { getCurrencies } from "../../../../services";

const Compare = ({
  amount,
  changeAmount,
  selectedCurrencies,
  setSelectedCurrencies,
  changeView,
}) => {
  const [currencies, setCurrencies] = useState([]);

  useEffect(() => {
    getCurrencies().then((response) => {
      const { GBP, USD, AUD, CAD, CHF, CNY, SEK, NZD } = response.rates;
      const currencyData = [
        { currency: "GBP", value: (GBP * amount).toFixed(2) },
        { currency: "USD", value: (USD * amount).toFixed(2) },
        { currency: "AUD", value: (AUD * amount).toFixed(2) },
        { currency: "CAD", value: (CAD * amount).toFixed(2) },
        { currency: "CHF", value: (CHF * amount).toFixed(2) },
        { currency: "CNY", value: (CNY * amount).toFixed(2) },
        { currency: "SEK", value: (SEK * amount).toFixed(2) },
        { currency: "NZD", value: (NZD * amount).toFixed(2) },
      ];
      setCurrencies(currencyData);
    });
  }, []);

  const clickHandler = (currency) => {
    const currentCurrencies = [...selectedCurrencies];
    const alreadySelected = currentCurrencies.findIndex(
      (selectedCurrency) => selectedCurrency === currency
    );
    if (alreadySelected !== -1) {
      currentCurrencies.splice(alreadySelected, 1);
    } else {
      if (selectedCurrencies.length > 1) {
        currentCurrencies.pop();
      }
      currentCurrencies.push(currency);
    }
    setSelectedCurrencies(currentCurrencies);
  };

  const numberReg = /^\d+$/;
  const isButtonDisabled =
    selectedCurrencies.length < 2 ||
    !amount ||
    amount <= 0 ||
    !numberReg.test(amount);

  return (
    <div className={styles.wrapper}>
      <div className={styles.currencyInputHeader}>
        <h3> Enter a value for EUR </h3>
        <div className={styles.inputContainer}>
          <input
            type={"number"}
            onChange={({ target }) => changeAmount(target.value)}
            type="text"
            value={amount}
          />
        </div>
      </div>

      <div className={styles.multiSelectContainer}>
        <h3>Select two...</h3>
        <div className={styles.multiSelectWrapper}>
          {currencies.map(({ currency, value }) => {
            const isSelected = selectedCurrencies.find(
              (selectedCurrency) => selectedCurrency === currency
            );
            return (
              <div
                style={isSelected && { backgroundColor: "lightGray" }}
                className={styles.option}
                onClick={() => {
                  clickHandler(currency);
                }}
              >
                <div className={styles.currency}>{currency}</div>
                <div className={styles.value}>{value}</div>
              </div>
            );
          })}
        </div>
      </div>
      <div className={styles.buttonContainer}>
        <Button
          clickHandler={() => changeView("result")}
          disabled={isButtonDisabled}
          text={"Compare"}
        />
      </div>
    </div>
  );
};

export default Compare;
