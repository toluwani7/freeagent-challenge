import React, { useState } from "react";
import { Compare, Result } from "./components";

const CurrencyTable = () => {
  const [currentView, setCurrentView] = useState("compare");
  const [selectedCurrencies, setSelectedCurrencies] = useState([]);
  const [amount, setAmount] = useState(100);

  //document to understand..

  const resetForm = () => {
    setAmount(100);
    setSelectedCurrencies([]);
    setCurrentView("compare");
  };

  return (
    <>
      {currentView === "compare" && (
        <Compare
          amount={amount}
          changeAmount={setAmount}
          selectedCurrencies={selectedCurrencies}
          setSelectedCurrencies={setSelectedCurrencies}
          changeView={setCurrentView}
        />
      )}
      {currentView === "result" && (
        <Result
          amount={amount}
          selectedCurrencies={selectedCurrencies}
          resetForm={resetForm}
        />
      )}
    </>
  );
};

export default CurrencyTable;
