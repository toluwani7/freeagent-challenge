import config from "../config";

const { API_URL, ACCESS_KEY } = config;

export const getCurrencies = async () => {
  try {
    const request = await fetch(`${API_URL}/latest?${ACCESS_KEY}`);
    const json = await request.json();
    return json;
  } catch (error) {
    console.log(error);
  }
};

export const getHistoricalData = async (currencies, startDate) => {
  try {
    // const endDate = moment().subtract(1, "days").format("YYYY-MM-DD");
    const currencyString = currencies.join(",");
    const url = `${API_URL}/${startDate}?${ACCESS_KEY}&base=EUR&symbols=${currencyString}`;
    console.log(url);
    const request = await fetch(url);
    const json = await request.json();
    return json;
  } catch (error) {
    console.log(error);
  }
};
