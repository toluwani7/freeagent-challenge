import React from "react";
import PropTypes from "prop-types";
import styles from "./styles.module.scss";

const Button = ({ text, clickHandler, disabled }) => (
  <div
    style={
      disabled
        ? {
            pointerEvents: "none",
            backgroundColor: "lightGray",
            border: "1px solid gray",
          }
        : null
    }
    className={styles.button}
    onClick={clickHandler}
  >
    {text}
  </div>
);

Button.defaultProps = {
  disabled: false,
  text: "Compare",
  clickHandler: () => {},
};

Button.propTypes = {
  disabled: PropTypes.bool,
  text: PropTypes.string,
  clickHandler: PropTypes.func,
};

export default Button;
